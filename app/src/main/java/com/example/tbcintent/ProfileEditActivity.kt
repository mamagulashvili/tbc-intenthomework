package com.example.tbcintent

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.text.TextUtils
import android.util.Patterns
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.widget.doOnTextChanged
import com.example.tbcintent.Constant.EXTRA_EMAIL
import com.example.tbcintent.Constant.EXTRA_GENDER
import com.example.tbcintent.Constant.EXTRA_LAST_NAME
import com.example.tbcintent.Constant.EXTRA_NAME
import com.example.tbcintent.Constant.EXTRA_YEAR
import com.example.tbcintent.Constant.IMAGE_REQUEST_CODE
import com.example.tbcintent.Constant.NAME_REGEX
import com.example.tbcintent.Constant.PERMISSION_REQUEST_CODE
import com.example.tbcintent.databinding.ActivityProfileEditBinding
import com.google.android.material.snackbar.Snackbar
import java.io.ByteArrayOutputStream
import java.util.regex.Pattern

class ProfileEditActivity : AppCompatActivity() {
    private lateinit var binding: ActivityProfileEditBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityProfileEditBinding.inflate(layoutInflater)
        setContentView(binding.root)
        init()
    }

    private fun init() {
        setSupportActionBar(binding.toolbar)
        dropDownMenuAdapters()
        inputValidation()
        val saveButton = binding.saveButton
        saveButton.isClickable = inputValidation()
        if (saveButton.isClickable) {
            saveButton.setOnClickListener {
                moveBackToProfilePage()
            }
        }
        binding.profileImageView.setOnClickListener {
            requestPermission()
        }
    }

    private fun dropDownMenuAdapters() {
        val years = resources.getStringArray(R.array.years)
        val gender = resources.getStringArray(R.array.gender)
        val yearsDropDownAdapter = ArrayAdapter(this, R.layout.spinner_item, years)
        val genderDropDownAdapter = ArrayAdapter(this, R.layout.spinner_item, gender)
        binding.apply {
            yearsTextView.setAdapter(yearsDropDownAdapter)
            genderTextView.setAdapter(genderDropDownAdapter)
        }
    }

    private fun moveBackToProfilePage() {
        val email = binding.emailEditText.text.toString().trim()
        val name = binding.nameEditText.text.toString().trim()
        val lastName = binding.lastNameEditText.text.toString().trim()
        val yearOfBirth = binding.yearsTextView.text.toString()
        val gender = binding.genderTextView.text.toString()
        if (email.isNotEmpty() && name.isNotEmpty() && lastName.isNotEmpty()
            && yearOfBirth.isNotEmpty() && gender.isNotEmpty()
        ) {
            if (isValidEmail(email)) {
                val intent = intent
                intent.apply {
                    putExtra(EXTRA_NAME, name)
                    putExtra(EXTRA_LAST_NAME, lastName)
                    putExtra(EXTRA_YEAR, yearOfBirth)
                    putExtra(EXTRA_GENDER, gender)
                    putExtra(EXTRA_EMAIL, email)
                }
                setResult(Activity.RESULT_OK, intent)
                finish()
            } else {
                Snackbar.make(
                    binding.profileEditContainer,
                    "Please Enter Correct Email Address",
                    Snackbar.LENGTH_SHORT
                ).apply {
                    setTextColor(Color.RED)
                }.show()
            }

        } else {
            Snackbar.make(
                binding.profileEditContainer,
                "Please Fill All Fields",
                Snackbar.LENGTH_SHORT
            ).apply {
                setTextColor(Color.RED)
            }.show()
        }

    }

    private fun grantedExternalStoragePermission() =
        ActivityCompat.checkSelfPermission(
            this,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        ) == PackageManager.PERMISSION_GRANTED

    private fun requestPermission() {
        val requestPermission = mutableListOf<String>()
        if (!grantedExternalStoragePermission()) {
            requestPermission.add(Manifest.permission.WRITE_EXTERNAL_STORAGE)
        }
        if (requestPermission.isNotEmpty()) {
            ActivityCompat.requestPermissions(this, requestPermission.toTypedArray(), PERMISSION_REQUEST_CODE)
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == PERMISSION_REQUEST_CODE && grantResults.isNotEmpty()) {
            for (i in grantResults.indices) {
                if (grantResults[i] == PackageManager.PERMISSION_GRANTED && permissions[i] == Manifest.permission.WRITE_EXTERNAL_STORAGE) {
                    getImage()
                } else {
                    requestPermission()
                }
            }
        }
    }

    private fun getImage() {
        Intent(Intent.ACTION_GET_CONTENT).also {
            it.type = "image/*"
            startActivityForResult(it, IMAGE_REQUEST_CODE)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == IMAGE_REQUEST_CODE){
            val uri = data?.data
            binding.profileImageView.setImageURI(uri)
        }
    }

    private fun inputValidation(): Boolean {
        var isClickable = true
        binding.apply {
            emailEditText.doOnTextChanged { text, _, _, _ ->
                if (!text?.let { isValidEmail(it) }!!) {
                    binding.emailEditTextContainer.error = "InCorrect Email"
                    isClickable = false
                } else {
                    binding.emailEditTextContainer.error = null

                }
            }
            nameEditText.doOnTextChanged { text, _, _, _ ->
                if (Pattern.matches(NAME_REGEX, text!!)) {
                    binding.nameEditTextContainer.error = null

                } else {
                    binding.nameEditTextContainer.error = "InCorrect Name"
                    isClickable = false

                }
            }
            lastNameEditText.doOnTextChanged { text, _, _, _ ->
                if (Pattern.matches(NAME_REGEX, text!!)) {
                    binding.lastNameEditTextContainer.error = null
                } else {
                    binding.lastNameEditTextContainer.error = "InCorrect LastName"
                    isClickable = false


                }
            }
        }
        return isClickable
    }

    private fun isValidEmail(email: CharSequence): Boolean {
        return !TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

}