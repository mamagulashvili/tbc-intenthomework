package com.example.tbcintent

import android.app.Activity
import android.content.Intent
import android.graphics.drawable.AnimationDrawable
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.example.tbcintent.Constant.EDIT_PAGE_REQUEST_CODE
import com.example.tbcintent.Constant.EXTRA_EMAIL
import com.example.tbcintent.Constant.EXTRA_GENDER
import com.example.tbcintent.Constant.EXTRA_LAST_NAME
import com.example.tbcintent.Constant.EXTRA_NAME
import com.example.tbcintent.Constant.EXTRA_YEAR
import com.example.tbcintent.databinding.ActivityProfileBinding

class ProfileActivity : AppCompatActivity() {
    private lateinit var binding: ActivityProfileBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityProfileBinding.inflate(layoutInflater)
        setContentView(binding.root)
        init()
    }

    private fun init() {
        setSupportActionBar(binding.toolbar)
        backgroundAnimation()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == EDIT_PAGE_REQUEST_CODE && resultCode == Activity.RESULT_OK){
            val name = data?.extras?.getString(EXTRA_NAME,"")
            val email = data?.extras?.getString(EXTRA_EMAIL,"")
            val gender = data?.extras?.getString(EXTRA_GENDER,"")
            val yearOfBirth = data?.extras?.getString(EXTRA_YEAR,"")
            val lastName = data?.extras?.getString(EXTRA_LAST_NAME,"")
            binding.apply {
                emailTextView.text = email
                nameTextView.text = name
                lastNameTextView.text = lastName
                genderTextView.text = gender
                yearOfBirthTextView.text = yearOfBirth
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_profile_items, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menu_edit_button -> moveToEditPage()
        }
        return true
    }

    private fun moveToEditPage() {
        Intent(this, ProfileEditActivity::class.java).also {
            startActivityForResult(it, EDIT_PAGE_REQUEST_CODE)
        }
    }

    private fun backgroundAnimation() {
        val drawableAnim = binding.profileContainer.background as AnimationDrawable
        drawableAnim.apply {
            setEnterFadeDuration(1300)
            setExitFadeDuration(3000)
            start()
        }
    }
}