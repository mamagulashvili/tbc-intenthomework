package com.example.tbcintent

object Constant {
    const val EDIT_PAGE_REQUEST_CODE = 1
    const val EXTRA_NAME = "extra_name"
    const val EXTRA_LAST_NAME = "extra_last_name"
    const val EXTRA_EMAIL = "extra_email"
    const val EXTRA_YEAR = "extra_year"
    const val EXTRA_GENDER = "extra_gender"
    const val NAME_REGEX = "^\\p{L}+[\\p{L}\\p{Z}\\p{P}]{0,}"
    const val IMAGE_REQUEST_CODE = 16
    const val PERMISSION_REQUEST_CODE = 0
}